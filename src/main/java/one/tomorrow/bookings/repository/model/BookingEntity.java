package one.tomorrow.bookings.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bookings")
public class BookingEntity {
    @Id
    private UUID id;
    private UUID userId;
    private UUID accountId;
    private BigDecimal amount;
    private ZonedDateTime bookingDate;
    private ZonedDateTime valueDate;
    private String beneficiary;
    private String booking;
    private String bookingDetail;
    private String iban;
    private String bic;
}
