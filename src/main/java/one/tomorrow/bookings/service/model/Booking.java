package one.tomorrow.bookings.service.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

@RequiredArgsConstructor
@Getter
public class Booking {
    private final UUID id;
    private final Amount amount;
    private final ZonedDateTime bookingDate;
    private final ZonedDateTime valueDate;
    private final String beneficiary;
    private final String bookingTitle;
    private final String bookingDetail;
    private final String iban;
    private final String bic;
}
