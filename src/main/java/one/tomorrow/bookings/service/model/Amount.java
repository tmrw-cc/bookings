package one.tomorrow.bookings.service.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Getter
public class Amount {
    private final BigDecimal value;
    private final Currency currency;

    public enum Currency {
        EUR
    }
}
