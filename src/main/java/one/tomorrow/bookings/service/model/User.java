package one.tomorrow.bookings.service.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Getter
public class User {
    private final UUID id;
    private final String userName;
    private final String name;
    private final String firstName;
}
