package one.tomorrow.bookings.service;

import lombok.RequiredArgsConstructor;
import one.tomorrow.bookings.repository.BookingsRepository;
import one.tomorrow.bookings.repository.model.BookingEntity;
import one.tomorrow.bookings.service.model.Amount;
import one.tomorrow.bookings.service.model.BookingSummary;
import one.tomorrow.bookings.service.model.User;
import one.tomorrow.bookings.util.SecurityHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class BookingsService {

    private final BookingsRepository bookingsRepository;

    public List<BookingSummary> getBookingSummaries() {
        User user = SecurityHelper.getCurrentAutheticatedUser();
        return bookingsRepository.findByUserId(user.getId()).stream()
                .map(this::createBookingSummary)
                .collect(Collectors.toList());
    }

    private BookingSummary createBookingSummary(BookingEntity entity) {
        return new BookingSummary(
                entity.getId(),
                new Amount(
                        entity.getAmount(),
                        Amount.Currency.EUR
                ),
                entity.getBookingDate(),
                entity.getValueDate(),
                entity.getBeneficiary(),
                entity.getBooking()
        );
    }
}
